@extends("layouts.app")

@section("content")
    <p>
        <b>Overview</b><br>
        You are required to create a blog using PHP Laravel Framework and HTML. A migrate script is already included in this repository.
        The structure, style of your code and the way you handle exceptions will be assessed.
    </p>

    <ol>
        <li>
            Requirements for this system include
            <ul>
                <li>Display a list of the blog posts</li>
                <li>Add/edit/view a blog post</li>
                <li>Delete posts from the blog list</li>
                <li>Search posts from the list</li>
            </ul>
        </li>
        <li>
            Process Flow
            <img src="{{ asset("img/program_flow.jpg") }}" width="100%">
        </li>
    </ol>
    <hr>
    <h2>Development Tasks</h2>
    <ol>
        <li>
            Run migration to add necessary table needed for the CRUD Application<br>
            <small>You can add more columns if you need too, as long as long as you use Laravel migrate.</small>
        </li>
        <li>
            Posts Listing
            <ol type="a">
                <li>List all existing record</li>
                <li>Limit pages by 5</li>
                <li>Sort posts newest first</li>
                <li>
                    List table contains the following:
                    <ul>
                        <li>Post # (format {year}-{3 Random Letter}-{Auto increment number, length is 4 and padded by zeroes 0's})<br>example: 2018-ABC-0001</li>                        
                        <li>Post Subject</li>
                        <li>Date Posted</li>
                        <li>View button</li>
                        <li><i>(optional) Posted by</i></li>
                    </ul>
                </li>
                <li><i>(optional) Allow user to change sorting by clicking headers</i></li>
                <li><i>(optional) Use ajax on sorting, paging and searching records</i></li>
            </ol>
        </li>
        <li>
            Post Creation
            <ul>
                <li>Allow user to enter his/her name</li>
                <li>Upon clicking save, all entered data wil be save to database.</li>
                <li>
                    Required field:
                    <ul>
                        <li>Writer</li>
                        <li>Subject</li>
                        <li>Body</li>
                    </ul>
                </li>
                <li>After saving, redirect user to view page.</li>
                <li>
                    add post number in the format {YEAR}-{3 Random Latter}-{Auto increment number, length is 4 and padded by zeroes 0's}
                    <br>
                    example: 2018-ABC-0001 and counter should go back to 0001 every year
                </li>
                <li><i>(optional) Allow user to upload images</i></li>                
            </ul>
        </li>
        <li>
            Viewing Post
            <ul>
                <li>Allow user to view posts</li>
                <li>On click on list button, user will be redirected to list page</li>
                <li>On click on edit button, user will be redirected to edit page</li>
                <li>Restrict post id for better security to the format given above</li>
            </ul>
        </li>
        <li>
            Editing Post
            <ul>
                <li>Allow user to change the writer's name</li>
                <li>Upon clicking save, update all records.</li>
                <li>
                    Required field:
                    <ul>
                        <li>Writer</li>
                        <li>Subject</li>
                        <li>Body</li>
                    </ul>
                </li>
                <li>After saving, redirect user to view page.</li>
                <li>Restrict post id for better security to the format given above</li>
                <li><i>(optional) Allow user to change or add attachement</i></li>
            </ul>
        </li>
    </ol>
@endsection("content")